package main

import (
	"github.com/gin-gonic/gin"

	"fmt"
	"os"
)

type Config struct {
	MongoURL string
	Port     string
	Records  int
}

var config Config

func main() {
	ConfigRuntime()

	sess := InitializeDB(config.MongoURL)
	defer sess.Close()

	StartGin()
}

func StartGin() {
	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()
	router.LoadHTMLGlob("resources/*.templ.html")
	router.Static("/static", "resources/static")

	router.GET("/", index)
	router.GET("/words", wordsGET)
	router.GET("/all", allGET)
	router.POST("/word", wordPOST)

	router.Run(fmt.Sprintf(":%s", config.Port))
}

func ConfigRuntime() {
	uri := os.Getenv("MONGOHQ_URL")
	if len(uri) == 0 {
		uri = "mongodb://localhost:27017/wordkeeper"
		fmt.Println("no connection string provided")
	}
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "3000"
		fmt.Println("no port number provided")
	}

	config = Config{
		MongoURL: uri,
		Port:     port,
		Records:  10,
	}
}
