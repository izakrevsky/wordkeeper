package main

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"fmt"
	"os"
)

var collection *mgo.Collection

type word struct {
	Id          bson.ObjectId
	Original    string
	Language    string
	Description string
}

func NewWord(original string, language string, description string) *word {
	return &word{
		Id:          bson.NewObjectId(),
		Original:    original,
		Language:    language,
		Description: description,
	}
}

func InitializeDB(uri string) *mgo.Session {
	sess, err := mgo.Dial(uri)
	if err != nil {
		fmt.Printf("Can't connect to mongo, go error %v\n", err)
		os.Exit(1)
	}
	sess.SetSafe(&mgo.Safe{})

	collection = sess.DB("wordkeeper").C("words")
	return sess
}

func InsertWord(doc word) {
	err := collection.Insert(doc)
	if err != nil {
		fmt.Printf("Can't insert document: %v\n", err)
		os.Exit(1)
	}
}

func AllWords(pattern string) []word {
	var results []word

	err := collection.
		Find(bson.M{}).
		All(&results)
	if err != nil {
		panic(err)
	}

	return results
}

func SearchWords(pattern string) []word {
	var results []word

	err := collection.
		Find(bson.M{"original": bson.M{"$regex": pattern}}).
		Limit(config.Records).
		Sort("-id").
		All(&results)
	if err != nil {
		panic(err)
	}

	return results
}
