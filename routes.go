package main

import (
	"github.com/gin-gonic/gin"

	"strings"
)

func index(c *gin.Context) {
	c.Redirect(301, "/words")
}

func wordsGET(c *gin.Context) {
	pattern := strings.TrimSpace(c.Query("pattern"))
	words := SearchWords(pattern)

	c.HTML(200, "index.templ.html", gin.H{
		"words": words,
	})
}

func allGET(c *gin.Context) {
	c.JSON(200, AllWords(""))
}

func wordPOST(c *gin.Context) {
	word := strings.Title(strings.TrimSpace(c.PostForm("word")))
	language := strings.ToUpper(strings.TrimSpace(c.PostForm("language")))
	description := strings.TrimSpace(c.PostForm("description"))

	validWord := len(word) > 1 && len(word) < 50
	validLang := len(language) > 1 && len(language) < 20
	validDesc := len(description) > 1 && len(description) < 500
	if validDesc && validLang && validWord {
		InsertWord(*NewWord(word, language, description))
	}

	c.Redirect(301, "/words")
}
